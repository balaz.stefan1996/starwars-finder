# StarWars Finder

## Before you  start
Create ```.env``` which is not part of remote branch so if you want to test it create in root of project ``.env`` file and insert following :

````bash

VITE_API_ENDPOINT="https://swapi.py4e.com/api/people"

````
## What I used
I used ``Pinia store`` to have it one place also it is not really necessary to have it , due to complexity of this example.
Just proof of concept I know how to use it.

All hardcoded text I placed into ``quasar-project/src/i18n/en-US/index.ts`` , for future not necessary to refactor , if we need translations.

I don't use any custom component which should pass props, not necessary whole component has less as 100 lines.

I used `` Quasar CLI`` for it , in AC was using Javascript and ``Vue2`` after discussion and clarification I used ``Vue3`` due to Vue2 supports gonna end this year ``12/2023``.

I also used ``Typescript``, it's quicker for me for developing, and have hints.

## Install the dependencies

```bash
yarn
# or
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

```bash
quasar dev
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration





// This is just an example,
// so you can safely delete all default props below

export default {
  navBar: {
    name: 'Heroes finder',
    caption: 'Find your hero',
  },
  searchBar: {
    heading: 'StarWars search',
    placeHolder: 'Name',
    defaultMessage: 'Enter search phrase',
    nothingFound: 'Nothing found',
  },
};

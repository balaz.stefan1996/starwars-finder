import { defineStore } from 'pinia';
import axios from 'axios';

interface IPerson {
  birth_year: string;
  created: string;
  edited: string;
  eye_color: string;
  films: string[];
  gender: string;
  hair_color: string;
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  skin_color: string;
  species: string[];
  starships: string[];
  url: string;
  vehicles: string[];
}

export const usePeopleStore = defineStore('people', {
  state: () => ({
    people: [] as IPerson[],
    isLoading: true,
  }),
  getters: {
    getCharacters(): IPerson[] {
      return this.people;
    },
  },
  actions: {
    async searchCharacters(query: string) {
      this.isLoading = true;
      try {
        const response = await axios.get(
          `${import.meta.env.VITE_API_ENDPOINT}?search=${query}`
        );
        this.people = response?.data?.results;
      } catch (e: unknown) {
        console.error(e);
        this.people = [];
      } finally {
        this.isLoading = false;
      }
    },
    clearPeople() {
      this.people = [];
    },
  },
});
